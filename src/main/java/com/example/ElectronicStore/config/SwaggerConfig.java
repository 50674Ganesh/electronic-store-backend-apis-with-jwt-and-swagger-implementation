package com.example.ElectronicStore.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;

//http://localhost:9091/swagger-ui/index.html
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        Docket docket =new Docket(DocumentationType.SWAGGER_2);
        docket.apiInfo(getApiInfo());

        docket.securityContexts(Arrays.asList(getSecurityContext()));
        docket.securitySchemes(Arrays.asList(getSchemes()));
        ApiSelectorBuilder select = docket.select();
        select.apis(RequestHandlerSelectors.any());
        select.paths((PathSelectors.any()));
        Docket build = select.build();

        return build;
    }

    private SecurityContext getSecurityContext() {
        SecurityContext context = SecurityContext
                .builder()
                .securityReferences(getSecurityReference())
                .build();
        return context;
    }

    private List<SecurityReference> getSecurityReference() {
        AuthorizationScope[] scopes = {new AuthorizationScope("Global", "Access Every Thing")};
        return Arrays.asList(new SecurityReference("JWT", scopes));
    }

    private ApiKey getSchemes() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private ApiInfo getApiInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "Electronic Store Backend : APIs",
                "This is Backend Project created by GANESH PAWAR",
                "1.0.0v",
                "https://www.gitlab.com",
                new Contact("Ganesh", "https://www.instagram.com/ganesh_p612", "pawarganesh1212@gmail.com"),
                        "License of APIs",
                        "https://www.google.com",
                        new ArrayDeque<>()

        );

        return apiInfo;
    }

}
