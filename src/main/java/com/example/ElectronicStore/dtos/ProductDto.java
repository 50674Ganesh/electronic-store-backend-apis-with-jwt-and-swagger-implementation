package com.example.ElectronicStore.dtos;

import com.example.ElectronicStore.entities.Category;
import com.example.ElectronicStore.validate.ImageNameValid;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ProductDto {

    private String productId;
    private String title;
    private String description;
    private int price;
    private int quantity;
    private int discountedPrice;
    private Date addedDate;
    private boolean live;
    private boolean stock;

    private String productImageName;
    private CategoryDto category;
}
