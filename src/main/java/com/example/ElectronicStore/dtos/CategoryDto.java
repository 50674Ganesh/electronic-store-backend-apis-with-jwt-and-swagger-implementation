package com.example.ElectronicStore.dtos;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDto {
    private String categoryId;
    @NotBlank(message = "Title is required..")
    @Size(min = 4, message = "Title must be more than 4 characters")
    private String title;
    @NotBlank(message = "description Required...")
    private String description;
    @NotBlank(message = "cover image Required...")
    private String coverImage;
}
