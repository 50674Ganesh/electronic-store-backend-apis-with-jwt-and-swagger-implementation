package com.example.ElectronicStore.dtos;

import com.example.ElectronicStore.entities.Cart;
import com.example.ElectronicStore.entities.Product;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CartItemDto {
    private int cartItemId;
    private ProductDto product;
    private  int quantity;
    private int totalPrice;

}
