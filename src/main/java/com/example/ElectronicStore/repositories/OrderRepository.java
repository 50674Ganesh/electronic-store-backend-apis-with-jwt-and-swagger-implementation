package com.example.ElectronicStore.repositories;

import com.example.ElectronicStore.entities.Order;
import com.example.ElectronicStore.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

       List<Order> findByUser(User user);

}
