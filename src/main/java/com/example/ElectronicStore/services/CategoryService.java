package com.example.ElectronicStore.services;

import com.example.ElectronicStore.dtos.CategoryDto;
import com.example.ElectronicStore.dtos.PageableResponse;

public interface CategoryService {

    //create
    public CategoryDto create(CategoryDto categoryDto);

    //update
    public CategoryDto update(CategoryDto categoryDto, String categoryId);

    //delete
    public void delete(String categoryId);

    //get all
   public PageableResponse<CategoryDto> getAll(int pageNumber, int pageSize, String sortBy, String sortDir);

    //get single category detail
    public CategoryDto get(String categoryId);
}
