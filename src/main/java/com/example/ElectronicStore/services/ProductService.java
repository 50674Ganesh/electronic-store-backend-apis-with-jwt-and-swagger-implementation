package com.example.ElectronicStore.services;

import com.example.ElectronicStore.dtos.PageableResponse;
import com.example.ElectronicStore.dtos.ProductDto;

import java.util.List;

public interface ProductService {
    //create
    public ProductDto create(ProductDto productDto);

    //update
    public ProductDto update(ProductDto productDto, String productId);

    //delete
    public void delete(String productId);

    //get single
    public ProductDto getSingle(String productId);

    //get all
    public PageableResponse<ProductDto> getAll(int pageNumber, int pagerSize, String sortBy, String sortDir);

    //get all: live
   public PageableResponse<ProductDto> getAllLive(int pageNumber, int pagerSize, String sortBy, String sortDir);

    //search product
   public PageableResponse<ProductDto> searchByTitle(String subTitle, int pageNumber, int pagerSize, String sortBy, String sortDir);

   //create product with category
     ProductDto createWithCategory(ProductDto productDto, String categoryId);

     //update category of product
    ProductDto updateCategory(String productId, String categoryId);

    //return product of given category
    PageableResponse<ProductDto> getAllOfCategory(String categoryId,int pageNumber,int pageSize, String sortBy, String sortDir);
}
