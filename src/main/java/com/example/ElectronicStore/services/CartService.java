package com.example.ElectronicStore.services;

import com.example.ElectronicStore.dtos.AddItemToCartRequest;
import com.example.ElectronicStore.dtos.CartDto;

public interface CartService {

    //add item to cart
    public CartDto addItemToCart(String userId, AddItemToCartRequest request);

    //remove item to cart.
    public void removeItemFromCart(String userId, int cartItem);

    //clear cart
    public void clearCart(String userId);

    CartDto getCartByUser(String userId);


}
