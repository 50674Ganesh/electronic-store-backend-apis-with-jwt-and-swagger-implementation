package com.example.ElectronicStore.services;


import com.example.ElectronicStore.dtos.PageableResponse;
import com.example.ElectronicStore.dtos.UserDto;
import com.example.ElectronicStore.entities.User;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    //create
    public UserDto createUser(UserDto userDTO);

    //update
    public UserDto updateUser(UserDto userDto, String UserId);


    //delete
    public void deleteUser(String userId) throws IOException;


    //get all users
    public PageableResponse<UserDto> getAllUser(int pageNumber, int pageSize, String sortBy, String sortDir);


    //get single user by id
    public UserDto getUserById(String userId);


    //get single user by email
    public UserDto getUserByEmail(String email);


    //serch user
    public List<UserDto> searchUser(String keyword);

//    Optional<User> findUserByEmailOptional(String email);


}
