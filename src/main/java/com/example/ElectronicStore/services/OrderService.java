package com.example.ElectronicStore.services;

import com.example.ElectronicStore.dtos.CreateOrderRequest;
import com.example.ElectronicStore.dtos.OrderDto;
import com.example.ElectronicStore.dtos.PageableResponse;

import java.util.List;

public interface OrderService {

    //create order
    public OrderDto createOrder(CreateOrderRequest orderDto);

    //remove order
    public void removeOrder(String orderId);

    //get orders from user
    public List<OrderDto> getOrderOfUser(String userId);

    //get order
    public PageableResponse<OrderDto> getOrders(int pageNumber,int pageSize, String sortBy, String sortDir);

    //other method related to order.
}
