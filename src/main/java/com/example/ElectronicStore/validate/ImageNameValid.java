package com.example.ElectronicStore.validate;

import javax.validation.Constraint;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ImageNameValidator.class)
public @interface ImageNameValid {

    //error message
    java.lang.String message() default "Invalid Image Name  ! ";

    //represent group of constraints.
    java.lang.Class<?>[] groups() default {};

   // additional information about annotation.
    java.lang.Class<? extends javax.validation.Payload>[] payload() default {};
}
